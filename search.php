<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>search.php</h1>
</section>
<section role="region" class="content-region">
    <div class="container">
        <div class="content-flex">
            
            <?php get_template_part('partials/sidebar', 'box'); ?>

            <div class="posts-container">
                <div class="items-list">
                <?php if ( have_posts() ) : ?>
                    <div class="items-list">
                    <?php
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                        $args = array( 
                            'post_type' => 'blog', 
                            'posts_per_page' => 2, 
                            'paged' => $paged,
                            'orderby' => 'date',
                            'order' => 'DESC',
                            's' => get_search_query(true),
                        );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();
                    ?>         
                        <?php get_template_part('partials/blog', 'list'); ?>
                    <?php
                        endwhile;
                    ?>
                    </div>        
                    <div class="pager">
                        <?php echo paginate_links( array(
                            'total' => $loop->max_num_pages,
                            'prev_text' => __('Předchozí'),
                            'next_text' => __('Další'),
                        )); ?>
                    </div>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>