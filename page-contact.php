<?php /* Template Name: Kontakt */ ?>


<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>page-contact.php</h1>
</section>
<section role="region" class="subpage-region">
    <div class="container">
        <div class="texts-container tiny-container" role="main">
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <h1><?php the_title();?></h1>
                    <?php the_content(); ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<section role="region" class="contact-social-region">
    <?php dynamic_sidebar( 'social_sites' ); ?>      
</section>
<section role="region" class="contact-form-region">
    <?php dynamic_sidebar( 'contactForm' ); ?>      
</section>


<?php get_footer(); ?>