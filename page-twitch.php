<?php /* Template Name: Twitch stránka */ ?>


<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>page-twitch.php</h1>
</section>
<section role="region" class="subpage-region">
    <div class="container">
        <div class="content" role="main">
            <?php if( twitch_stream_live('fattypillow') ){ ?>
                <h1 style="color:green;">Stream je online</h1>
            <?php } else { ?>
                <h1 style="color:red;">Stream je offline</h1>
            <?php } ?>
                
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <h1><?php the_title();?></h1>
                    <?php the_content(); ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>