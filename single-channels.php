<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>single-channels.php</h1>
</section>
<section role="region" class="content-region">
    <div class="container">
        <div class="content-flex">
            
            <?php get_template_part('partials/sidebar', 'boxChannels'); ?>

            <div class="posts-container">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="post-detail">
                            <h1><?php the_title();?></h1>
                            <?php the_content(); ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>