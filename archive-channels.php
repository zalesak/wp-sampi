<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>archiv-channels.php</h1>
</section>
<section role="region" class="content-region">
    <div class="container">
        <div class="content-flex">
            
            <?php get_template_part('partials/sidebar', 'boxChannels'); ?>

            <div class="posts-container">
             
                <?php if ( have_posts() ) : ?>
                    <div class="items-list">
                    <?php
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                        $args = array( 
                            'post_type' => 'channels', 
                            'posts_per_page' => 10, 
                            'paged' => $paged,
                            //'orderby' => 'date',
                            'order' => 'DESC'
                        );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();
                    ?>         
                        <?php get_template_part('partials/channels', 'list'); ?>
                    <?php
                        endwhile;
                    ?>
                    </div>        
                    <div class="pager">
                        <?php echo paginate_links( array(
                            'total' => $loop->max_num_pages,
                            'prev_text' => __('Předchozí'),
                            'next_text' => __('Další'),
                        )); ?>
                    </div>
                <?php endif; ?>
          
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>