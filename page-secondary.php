<?php /* Template Name: O nás */ ?>


<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>page-secondary.php</h1>
</section>
<section role="region" class="subpage-region">
    <div class="container">
        <div class="texts-container tiny-container" role="main">
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <h1><?php the_title();?></h1>
                    <?php the_content(); ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<section role="region" class="achievments-region" id="achiev-waypoint">
    <div class="container"> 
        <div class="achievments-container">       
            <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                <div class="achiev-title">Awards:</div>
            <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
                <div class="achiev-title">Ocenění:</div>
            <?php endif; ?>                        
            <?php dynamic_sidebar( 'pocetZlatych' ); ?>                             
            <?php dynamic_sidebar( 'pocetStribrnych' ); ?>                             
            <?php dynamic_sidebar( 'pocetBronzovych' ); ?>     
            <div class="achiev-total">
                <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                    <div class="total-matches">Total awards:</div>
                <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
                    <div class="total-matches">Celkem zápasů:</div>
                <?php endif; ?>     
                <?php dynamic_sidebar( 'pocetZapasu' ); ?>            
            </div>                        
        </div>
    </div>
</section>
<section role="region" class="half-photos-region">
    <?php dynamic_sidebar( 'oNasGalerie' ); ?>         
</section>


<?php get_footer(); ?>