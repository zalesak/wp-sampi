<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>single-teams.php</h1>
</section>
<section role="region" class="content-region">
    <div class="container">
        <div class="content-flex">
            <div class="posts-container">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="post-detail">
                            <p class="post-detail-category">
                                <?php
                                    $terms = get_the_terms( $post->ID , 'teamscat' );
                                    foreach ( $terms as $term ) {
                                        echo '<a href="'.get_term_link($term->slug, 'teamscat').'">'.$term->name.'</a>';
                                    }
                                ?>
                            </p>
                            <h1><?php the_title();?></h1>
                            <?php the_content(); ?>

                            <?php $teamsFacebook = get_post_meta( $post->ID, 'teamsFacebook', true ); ?>
                            <?php if( ! empty( $teamsFacebook ) ) : ?>
                                <div class="social-site-link facebook-icon">
                                    <a target="_blank" rel="nofollow" href="<?php echo get_post_meta( $post->ID, 'teamsFacebook', true ); ?>">
                                        <svg viewBox="0 0 512 512" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" version="1.1" width="26px" height="26px" fill="#3b5998">
                                            <path d="M288,176v-64c0-17.664,14.336-32,32-32h32V0h-64c-53.024,0-96,42.976-96,96v80h-64v80h64v256h96V256h64l32-80H288z"></path>
                                        </svg>
                                        Facebook
                                    </a>
                                </div>
                            <?php endif; ?>

                            <?php $teamsTwitter = get_post_meta( $post->ID, 'teamsTwitter', true ); ?>
                            <?php if( ! empty( $teamsTwitter ) ) : ?>
                            <div class="social-site-link twitter-icon">
                                <a target="_blank" rel="nofollow" href="<?php echo get_post_meta( $post->ID, 'teamsTwitter', true ); ?>">
                                    <svg viewBox="0 0 612 612" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" version="1.1" width="26px" height="26px" fill="#55acee">
                                        <path d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411
			                                c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513
			                                c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101
			                                c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104
			                                c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194
			                                c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485
			                                c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z">
                                        </path>
                                    </svg>
                                    Twitter
                                </a>
                            </div>
                            <?php endif; ?>

                            <?php $teamsInstagram = get_post_meta( $post->ID, 'teamsInstagram', true ); ?>
                            <?php if( ! empty( $teamsInstagram ) ) : ?>
                            <div class="social-site-link instagram-icon">
                                <a target="_blank" rel="nofollow" href="<?php echo get_post_meta( $post->ID, 'teamsInstagram', true ); ?>">
                                    <svg viewBox="0 0 512 512" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" version="1.1" width="26px" height="26px" fill="#517fa4">
                                        <path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160
			                                C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48
			                                h192c61.76,0,112,50.24,112,112V352z">
                                        </path>
                                        <path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336
			                                c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z">
                                        </path>
                                        <circle r="17.056" cy="118.4" cx="393.6"></circle>
                                    </svg>
                                    Instagram
                                </a>
                            </div>
                            <?php endif; ?>

                            <?php $teamsYoutube = get_post_meta( $post->ID, 'teamsYoutube', true ); ?>
                            <?php if( ! empty( $teamsYoutube ) ) : ?>
                            <div class="social-site-link youtube-icon">
                                <a target="_blank" rel="nofollow" href="<?php echo get_post_meta( $post->ID, 'teamsYoutube', true ); ?>">
                                    <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" version="1.1" width="26px" height="26px" fill="#FF0000">
                                        <path d="M490.24,113.92c-13.888-24.704-28.96-29.248-59.648-30.976C399.936,80.864,322.848,80,256.064,80                                    c-66.912,0-144.032,0.864-174.656,2.912c-30.624,1.76-45.728,6.272-59.744,31.008C7.36,138.592,0,181.088,0,255.904                                    C0,255.968,0,256,0,256c0,0.064,0,0.096,0,0.096v0.064c0,74.496,7.36,117.312,21.664,141.728                                    c14.016,24.704,29.088,29.184,59.712,31.264C112.032,430.944,189.152,432,256.064,432c66.784,0,143.872-1.056,174.56-2.816                                    c30.688-2.08,45.76-6.56,59.648-31.264C504.704,373.504,512,330.688,512,256.192c0,0,0-0.096,0-0.16c0,0,0-0.064,0-0.096                                    C512,181.088,504.704,138.592,490.24,113.92z M192,352V160l160,96L192,352z">
                                        </path>
                                    </svg>
                                    Youtube
                                </a>
                            </div>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="posts-container posts-container--grayed single-sidebar">
                <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                    <div class="container-main-title">Achievements</div>
                <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
                    <div class="container-main-title">Ocenění</div>
                <?php endif; ?>
                <div class="oceneni-list">
                    <?php
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                        $queried_object = get_post_meta( $post->ID, 'teamsOceneni', true );
                        if(ICL_LANGUAGE_CODE=='cs'){
                            $cat_name = $queried_object;
                        }elseif(ICL_LANGUAGE_CODE=='en'){
                            $cat_name = 'en-'.$queried_object;
                        }
                        $args = array( 
                            'post_type' => 'oceneni', 
                            'posts_per_page' => 10, 
                            'paged' => $paged,
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'ocenenicat' => $cat_name
                        );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();
                    ?>         
                        <?php get_template_part('partials/oceneni', 'list'); ?>
                    <?php
                        endwhile;
                    ?>
                </div>
            </div>                                        
        </div>
    </div>
</section>

<?php get_footer(); ?>