<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>taxonomy-eventcat.php</h1>
</section>
<section role="region" class="subpage-region">
    <div class="container">

            
            <?php get_template_part('partials/sidebar', 'boxEvents'); ?>

            <div class="posts-container">
             
             
                    <div class="event-archive-items">
                    <?php
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                        $queried_object = get_queried_object();
                        if(ICL_LANGUAGE_CODE=='cs'){
                            $cat_name = $queried_object->name;
                        }elseif(ICL_LANGUAGE_CODE=='en'){
                            $cat_name = 'en-'.$queried_object->name;
                        }
                        $args = array( 
                            'post_type' => 'events', 
                            'posts_per_page' => 12, 
                            'paged' => $paged,
                            'orderby' => 'date',
                            'order' => 'ASC',
                            'eventcat' => $cat_name,
                            'post_status' => array('publish','future')
                        );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();
                    ?>         
                        <?php get_template_part('partials/event', 'list'); ?>
                    <?php
                        endwhile;
                    ?>
                    </div>        
                    <div class="pager">
                        <?php echo paginate_links( array(
                            'total' => $loop->max_num_pages,
                            'prev_text' => __('Předchozí'),
                            'next_text' => __('Další'),
                        )); ?>
                    </div>
              
          
            </div>
  
    </div>
</section>

<?php get_footer(); ?>