<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>taxonomy-teamscat.php</h1>
</section>
<section role="region" class="content-region">
    <div class="container">
        <div class="content-flex">
            <div class="posts-container">

            <h1 class="container-main-title"><?php echo get_queried_object()->name ?></h1>

            <?php if ( have_posts() ) : ?>
                <div class="teams-list-container">
                <?php
                    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                    $queried_object = get_queried_object();
                    if(ICL_LANGUAGE_CODE=='cs'){
                        $cat_name = $queried_object->name;
                    }elseif(ICL_LANGUAGE_CODE=='en'){
                        $cat_name = 'en-'.$queried_object->name;
                    }
                    $args = array( 
                        'post_type' => 'teams', 
                        'posts_per_page' => 100, 
                        'paged' => $paged,
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'teamscat' => $cat_name
                    );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();
                ?>         
                    <?php get_template_part('partials/teams', 'list'); ?>
                <?php
                    endwhile;
                ?>
                </div>        
                <div class="pager">
                    <?php echo paginate_links( array(
                        'total' => $loop->max_num_pages,
                        'prev_text' => __('Předchozí'),
                        'next_text' => __('Další'),
                    )); ?>
                </div>
            <?php endif; ?>
            </div>
         
                    
            <div class="posts-container posts-container--grayed oceneni-list-container">
                <?php get_template_part('partials/sidebar', 'playerOceneni'); ?>
            </div>

        </div>
    </div>
</section>
<section role="region" class="subpage-region more-padding-here">
    <div class="container">
        <?php if(ICL_LANGUAGE_CODE=='en'): ?>
            <div class="container-main-title">Events</div>
        <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
            <div class="container-main-title">Události</div>
        <?php endif; ?>
        <div class="event-archive-items">
        <?php
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            $queried_object = get_queried_object();
            if(ICL_LANGUAGE_CODE=='cs'){
                $cat_name = $queried_object->name;
            }elseif(ICL_LANGUAGE_CODE=='en'){
                $cat_name = 'en-'.$queried_object->name;
            }
            $args = array( 
                'post_type' => 'events', 
                'posts_per_page' => 4, 
                'paged' => $paged,
                'orderby' => 'date',
                'order' => 'ASC',
                'eventcat' => $cat_name,
                'post_status' => array('publish','future')
            );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
        ?>         
            <?php get_template_part('partials/event', 'list'); ?>
        <?php
            endwhile;
        ?>
        </div>
    </div>
</section>
<section role="region" class="subpage-region">
    <div class="container">
        <?php if(ICL_LANGUAGE_CODE=='en'): ?>
            <div class="container-main-title">Gallery</div>
        <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
            <div class="container-main-title">Galerie</div>
        <?php endif; ?>
        <div class="blog-archive-items">
        <?php
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            $queried_object = get_queried_object();
            if(ICL_LANGUAGE_CODE=='cs'){
                $cat_name = $queried_object->name;
            }elseif(ICL_LANGUAGE_CODE=='en'){
                $cat_name = 'en-'.$queried_object->name;
            }
            $args = array( 
                'post_type' => 'galerie', 
                'posts_per_page' => 4, 
                'paged' => $paged,
                'orderby' => 'date',
                'order' => 'DESC',
                'galeriecat' => $cat_name
            );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
        ?>         
            <?php get_template_part('partials/galerie', 'list'); ?>
        <?php
            endwhile;
        ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>