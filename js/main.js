jQuery(document).ready(function($)
{
    $('.stream-popup-toggle').click(function() {
        $('.header-channels-list').slideToggle('fast');          
    });

    $('.menu-toggler').on('click', function () {
        $(this).toggleClass('open');
        $('.head-menu').toggleClass('open');
    });

    $("#getting-started").countdown("2019/01/30 21:45:00", function(event) {
         $(this).text(
            event.strftime('%H:%M:%S')
         );
         $('body').addClass('overflow');    
         $(this).on('finish.countdown', function() {
             $('body').removeClass('overflow');    
             $('#getting-started').hide();    
         })
    });

    $(".menu-item-has-children ul").hide();
    $(".menu-item-has-children > a").attr("href", "javascript:void()");
    $(".menu-item-has-children > a").click(function() {
        $(this).next("ul").toggle();
    });
    
    $('.gallery').find('br').remove();

    $('#achiev-waypoint').waypoint(function(direction) {
        $('.achievments-container>.widget_text>.textwidget').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });

        $('.achiev-total>.widget_text>.textwidget').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });

        this.destroy()
    }, { offset: 700 });
});

