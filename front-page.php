<?php get_header('home'); ?>

<section role="region" class="home-header">
    <div class="home-header-container">
    <div class="highlighted-items-list">
        <?php
            $argsBlogBig = array( 
                'post_type' => 'blog', 
                'posts_per_page' => 3, 
                'orderby' => 'date',
                'order' => 'DESC'
            );
            $loopBlogBig = new WP_Query( $argsBlogBig );
            while ( $loopBlogBig->have_posts() ) : $loopBlogBig->the_post();
        ?>         
            <?php get_template_part('partials/blog', 'listBig'); ?>
        <?php
            endwhile;
        ?>
    </div>
    </div>
</section>
<section role="region" class="content-region">
    <div class="container">
        <div class="content-flex">

            <div class="posts-container">
                <div class="container-main-title">News</div>
                <div class="blog-list">
                    <?php
                        $argsBlog = array( 
                            'post_type' => 'blog', 
                            'posts_per_page' => 4, 
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'offset' => 3
                        );
                        $loopBlog = new WP_Query( $argsBlog );
                        while ( $loopBlog->have_posts() ) : $loopBlog->the_post();
                    ?>         
                        <?php get_template_part('partials/blog', 'list'); ?>
                    <?php
                        endwhile;
                    ?>
                </div>
                <div class="item-list-button text-center">
                    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                        <a href="/news">archive</a>
                    <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
                        <a href="/blog">archiv</a>
                    <?php endif; ?>
                </div>
            </div>

            <div class="posts-container posts-container--grayed">
                <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                    <div class="container-main-title">Events</div>
                <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
                    <div class="container-main-title">Události</div>
                <?php endif; ?>
                <div class="events-list">
                    <?php
                        $argsEvents = array( 
                            'post_type' => 'events', 
                            'posts_per_page' => 4, 
                            'orderby' => 'date',
                            'order' => 'ASC',
                            'post_status' => array('publish','future')
                        );
                        $loopEvents = new WP_Query( $argsEvents );
                        while ( $loopEvents->have_posts() ) : $loopEvents->the_post();
                    ?>         
                        <?php get_template_part('partials/event', 'list'); ?>
                    <?php
                        endwhile;
                    ?>
                </div>
                <div class="item-list-button">
                    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                        <a href="/events">archive</a>
                    <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
                        <a href="/events">archiv</a>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
</section>
<section role="region" class="about-region">
    <div class="container">
        <div class="about-container">       
            <?php dynamic_sidebar( 'about' ); ?>                 
        </div>
    </div>
</section>
<section role="region" class="achievments-region" id="achiev-waypoint">
    <div class="container"> 
        <div class="achievments-container">       
            <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                <div class="achiev-title">Awards:</div>
            <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
                <div class="achiev-title">Ocenění:</div>
            <?php endif; ?>                        
            <?php dynamic_sidebar( 'pocetZlatych' ); ?>                             
            <?php dynamic_sidebar( 'pocetStribrnych' ); ?>                             
            <?php dynamic_sidebar( 'pocetBronzovych' ); ?>     
            <div class="achiev-total">
                <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                    <div class="total-matches">Total awards:</div>
                <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
                    <div class="total-matches">Celkem zápasů:</div>
                <?php endif; ?>     
                <?php dynamic_sidebar( 'pocetZapasu' ); ?>            
            </div>                        
        </div>
    </div>
</section>

<section role="region" class="social-region">
    <div class="container">
        <h2>Socials Live</h2>
        <?php dynamic_sidebar( 'curator' ); ?>          
    </div>
</section>
<section role="region" class="sponzors-region">
    <div class="container">
        <h2>Sponzors</h2>
        <?php dynamic_sidebar( 'sponzors' ); ?>
    </div>
</section>

<?php get_footer(); ?>