<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>single-events.php</h1>
</section>
<section role="region" class="content-region">
    <div class="container">
        <div class="content-flex">
            <div class="posts-container">
                
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="post-detail">
                            <h1><?php the_title();?></h1>
                            <div class="post-detail-date"><?php echo get_the_date('j. F Y - G:i'); ?></div>
                            <?php the_content(); ?>
                        </div>
                    <?php endwhile; ?>
                
            </div>
            <div class="posts-container posts-container--grayed single-sidebar">
                <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                    <div class="container-main-title">Events</div>
                <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
                    <div class="container-main-title">Události</div>
                <?php endif; ?>
                <div class="events-list">
                    <?php
                        $argsEvents = array( 
                            'post_type' => 'events', 
                            'posts_per_page' => 4, 
                            'orderby' => 'date',
                            'order' => 'ASC',
                            'post_status' => array('publish','future')
                        );
                        $loopEvents = new WP_Query( $argsEvents );
                        while ( $loopEvents->have_posts() ) : $loopEvents->the_post();
                    ?>         
                        <?php get_template_part('partials/event', 'list'); ?>
                    <?php
                        endwhile;
                    ?>
                </div>
            </div>                                        
        </div>
    </div>
</section>

<?php get_footer(); ?>