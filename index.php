<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>index.php</h1>
</section>
<section role="region" class="subpage-region">
    <div class="container">
        <div class="content" role="main">
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <h1><?php the_title();?></h1>
                <?php the_content(); ?>
            <?php endwhile; ?>
        <?php endif; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>