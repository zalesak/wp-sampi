<?php get_header(); ?>

<section role="region" class="subpage-secondary-header">
    <h1>single-blog.php</h1>
</section>
<section role="region" class="content-region">
    <div class="container">
        <div class="content-flex">
            <div class="posts-container">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="post-detail">
                            <p class="post-detail-category">
                                <?php
                                    $terms = get_the_terms( $post->ID , 'galeriecat' );
                                    foreach ( $terms as $term ) {
                                        echo '<a href="'.get_term_link($term->slug, 'galeriecat').'">'.$term->name.'</a>';
                                    }
                                ?>
                            </p>
                            <h1><?php the_title();?></h1>
                            <div class="post-detail-date"><?php echo get_the_date('j. F Y'); ?></div>
                            <?php the_content(); ?>

                            <div class="post-detail-social">
                                <div class="post-detail-social-title">Do you like it? Share it!</div>
                                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4ffd33eb63205b4a"></script>
                                <div class="addthis_inline_share_toolbox"></div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="posts-container posts-container--grayed single-sidebar">
                <?php get_template_part('partials/sidebar', 'posts'); ?>
            </div>                                        
        </div>
    </div>
</section>

<?php get_footer(); ?>