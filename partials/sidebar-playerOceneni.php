
<?php if(ICL_LANGUAGE_CODE=='en'): ?>
    <div class="container-main-title">Achievements</div>
<?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
    <div class="container-main-title">Ocenění</div>
<?php endif; ?>
<div class="oceneni-list">
    <?php
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        $queried_object = get_queried_object();
        if(ICL_LANGUAGE_CODE=='cs'){
            $cat_name = $queried_object->name;
        }elseif(ICL_LANGUAGE_CODE=='en'){
            $cat_name = 'en-'.$queried_object->name;
        }
        $args = array( 
            'post_type' => 'oceneni', 
            'posts_per_page' => 100, 
            'paged' => $paged,
            'orderby' => 'date',
            'order' => 'DESC',
            'ocenenicat' => $cat_name
        );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
    ?>         
        <?php get_template_part('partials/oceneni', 'list'); ?>
    <?php
        endwhile;
    ?>
</div>