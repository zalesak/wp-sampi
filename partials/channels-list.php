<article role="article" class="post-item">                   
    <a target="_blank" rel="nofollow" class="item-link" href='<?php echo get_post_meta( $post->ID, 'channelUrl', true ) ?>' title="<?php the_title_attribute() ?>">
        <div class="item-img"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('thumbnail');} ?></div>
        <div class="item-info">
            <h2 class="item-title"><?php the_title();?></h2>
            <p><?php echo get_post_meta( $post->ID, 'channelUrl', true ); ?></p>
            <?php if( twitch_stream_live(get_post_meta( $post->ID, 'channelName', true )) ){ ?>
                <h3 style="color:#00b359;">Stream je online</h3>
            <?php } else { ?>
                <h3 style="color:#e80542;">Stream je offline</h3>
            <?php } ?>
        </div>
    </a>
</article>