<article role="article" class="oceneni-item">                   
    <div class="item-link">
        <div class="item-info">
            <div class="item-trophy place-<?php echo get_post_meta( $post->ID, 'oceneniPlace', true ); ?>">
                <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                    <div class="item-place"><?php echo get_post_meta( $post->ID, 'oceneniPlace', true ); ?>. place</div>
                <?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
                    <div class="item-place"><?php echo get_post_meta( $post->ID, 'oceneniPlace', true ); ?>. místo</div>
                <?php endif; ?>
            </div>
            <h2 class="item-title">
                <?php $gallery_url = get_post_meta( $post->ID, 'oceneniGallery', true ); ?>
                <?php if( ! empty( $gallery_url ) ) : ?>
                    <a href="<?php echo $gallery_url;  ?>"><?php the_title(); ?></a>
                <?php else: ?>
                    <?php the_title(); ?>
                <?php endif; ?>
            </h2>
            <div class="item-date"><?php echo get_the_date('F Y'); ?></div>
        </div>
    </div>
</article>