<article role="article" class="post-item">                   
    <a class="item-link" href='<?php the_permalink() ?>' title="<?php the_title_attribute() ?>">
        <div class="item-img"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('blogListBig');} ?></div>
        <div class="item-category
            <?php
                $terms = get_the_terms( $post->ID , 'blogcat' );
                foreach ( $terms as $term ) {
                    $term->name = preg_replace('/\s+/', '_', $term->name);
                    echo $term->name;
                }
            ?>
        ">
            <?php
                $terms = get_the_terms( $post->ID , 'blogcat' );
                foreach ( $terms as $term ) {
                    echo $term->name;
                }
            ?>
        </div>
        <div class="item-info">
            <div class="item-date"><?php echo get_the_date('j. n. Y'); ?></div>
            <h2 class="item-title"><?php the_title();?></h2>
        </div>
    </a>
</article>