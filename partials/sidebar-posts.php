<?php if(ICL_LANGUAGE_CODE=='en'): ?>
    <div class="sidebar-items-title">Latest news</div>
<?php elseif(ICL_LANGUAGE_CODE=='cs'): ?>
    <div class="sidebar-items-title">Poslední články</div>
<?php endif; ?>

<div class="sidebar-items-list">
    <?php
        $argsSide = array( 
            'post_type' => 'blog', 
            'posts_per_page' => 5, 
            'orderby' => 'date',
            'order' => 'DESC'
        );
        $loopSide = new WP_Query( $argsSide );
        while ( $loopSide->have_posts() ) : $loopSide->the_post();
    ?>         
        <article role="article" class="post-item">                   
            <a class="item-link" href='<?php the_permalink() ?>' title="<?php the_title_attribute() ?>">
                <div class="item-img">
                    <div class="item-category
                        <?php
                            $terms = get_the_terms( $post->ID , 'blogcat' );
                            foreach ( $terms as $term ) {
                                $term->name = preg_replace('/\s+/', '_', $term->name);
                                echo $term->name;
                            }
                        ?>
                    ">
                        <?php
                            $terms = get_the_terms( $post->ID , 'blogcat' );
                            foreach ( $terms as $term ) {
                                echo $term->name;
                            }
                        ?>
                    </div>
                    <?php if ( has_post_thumbnail() ) {the_post_thumbnail('thumbnail');} ?>
                </div>
                <div class="item-info">
                    <h2 class="item-title"><?php the_title();?></h2>
                    <div class="item-date"><?php echo get_the_date('j. F Y - G:i'); ?></div>
                    <p class="item-desc"><?php echo(get_the_excerpt()); ?></p>
                </div>
            </a>
        </article>
    <?php
        endwhile;
    ?>
</div>