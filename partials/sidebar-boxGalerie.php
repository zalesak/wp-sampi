<div class="topbar">
    <div class="topbar-categories">
        <nav class="topbar-categories-list">
            <ul>
                <?php wp_list_categories( array(
                    'orderby'    => 'name',
                    'show_count' => false,
                    'title_li' => '',
                    'taxonomy' => 'galeriecat',
                    'exclude'    => array( 1 )
                ) ); ?> 
            </ul>
        </nav>
    </div>
</div>