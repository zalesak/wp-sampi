<article role="article" class="post-item">                   
    <a class="item-link" href='<?php the_permalink() ?>' title="<?php the_title_attribute() ?>">
        <div class="item-img"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('teamList');} ?></div>
        <div class="item-info">
            <h2 class="item-title"><?php the_title();?></h2>
        </div>
    </a>
</article>