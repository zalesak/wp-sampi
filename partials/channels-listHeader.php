<article role="article" class="channel-item">                   
    <a target="_blank" rel="nofollow" class="item-link" href='<?php echo get_post_meta( $post->ID, 'channelUrl', true ) ?>' title="<?php the_title_attribute() ?>">
        <div class="item-img"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('thumbnail');} ?></div>
        <div class="item-info">
            <h2 class="item-title"><?php the_title();?></h2>
            <div class="item-url"><?php echo get_post_meta( $post->ID, 'channelUrl', true ); ?></div>
            <?php if( twitch_stream_live(get_post_meta( $post->ID, 'channelName', true )) ){ ?>
                <div class="item-status" style="color:green;">Stream je online</div>
            <?php } else { ?>
                <div class="item-status" style="color:red;">Stream je offline</div>
            <?php } ?>
        </div>
    </a>
</article>