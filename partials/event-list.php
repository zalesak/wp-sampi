<article role="article" class="event-item">                   
    <a class="item-link ico-<?php
                    $terms = get_the_terms( $post->ID , 'eventcat' );
                    foreach ( $terms as $term ) {
                        $term->name = preg_replace('/\s+/', '_', $term->name);
                        echo $term->name;
                    }
                ?>" href='<?php the_permalink() ?>' title="<?php the_title_attribute() ?>">
        <div class="item-img"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('blogList');} ?></div>
        <div class="item-info">
            <div class="item-date"><?php echo get_the_date('j. F Y - G:i'); ?></div>
            <p class="item-category">
                <?php
                    $terms = get_the_terms( $post->ID , 'eventcat' );
                    foreach ( $terms as $term ) {
                        echo $term->name;
                    }
                ?>
            </p>
            <h2 class="item-title"><?php the_title();?></h2>
            <p class="item-desc"><?php echo(get_the_excerpt()); ?></p>
            <div class="item-type"><?php echo get_post_meta( $post->ID, 'eventType', true ); ?></div>
        </div>
    </a>
</article>