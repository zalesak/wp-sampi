<!DOCTYPE html>
<html lang="cs">
<head>
    <title>Sampi</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author" content="Zalesak 2.0" />
    <meta name="rating" content="General" /> 
    <meta name="robots" content="noindex">
    <meta name="revisit-after" content="1 Day" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <?php wp_head(); ?>
</head>
<body>
    <div class="wrapper">
        <header class="header header-homepage clearfix" role="header">
            <div class="header-container clearfix">
                <nav class="head-menu">
                    <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
                    <button type="button" class="header-stream-button stream-popup-toggle">Stream</button>
                    <div class="social-mobile"><?php dynamic_sidebar( 'social_sites' ); ?></div>
                </nav>
                <button type="button" class="mobile-stream-button stream-popup-toggle">Stream</button>

                <?php dynamic_sidebar( 'social_sites' ); ?>
                <?php dynamic_sidebar( 'language' ); ?>

                <button type="button" class="menu__btn menu-toggler burger"><span></span></button>
            </div>
            <div class="header-channels-list">
                <div class="channels-container">
                    <?php
                        $argsHeaderChannels = array( 
                            'post_type' => 'channels', 
                            'posts_per_page' => 999, 
                            'orderby' => 'date',
                            'order' => 'DESC'
                        );
                        $loopHeaderChannels = new WP_Query( $argsHeaderChannels );
                        while ( $loopHeaderChannels->have_posts() ) : $loopHeaderChannels->the_post();
                    ?>         
                        <?php get_template_part('partials/channels', 'listHeader'); ?>
                    <?php
                        endwhile;
                    ?>
                </div>
            </div>
            <div class="header-intro">
                <div class="header-logo home-header-logo">
                    <a href="/" title="Přejít na úvodní stránku">SAMPI</a>
                </div>
                <div class="header-logo-caption">playing is in our DNA
                    <div class="hlc-icon">
                        <svg
                        xmlns:svg="http://www.w3.org/2000/svg"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 396 396"
                        height="12"
                        width="12"
                        version="1.1">
                        <path d="m 190,394.70288 c -6.16753,-2.1746 -144.422297,-75.39663 -152.270969,-80.64524 -7.863951,-5.25884 -14.20731,-14.23291 -16.30514,-23.06721 -1.232755,-5.19134 -4.397266,-83.97011 -3.417328,-85.07256 0.198432,-0.22324 41.159329,21.22676 91.024217,47.66666 49.86489,26.43991 91.39207,48.07256 92.28263,48.07256 1.45872,0 37.03718,-21.17845 43.22447,-25.72984 2.32756,-1.71214 0.39373,-3.00449 -23.33333,-15.59336 -14.22916,-7.54956 -29.35519,-15.65348 -33.61338,-18.00869 l -7.74216,-4.2822 43.42272,-26.31799 43.42272,-26.31797 46.31944,24.45836 c 52.39743,27.66776 58.62915,32.22243 64.1026,46.85155 3.50548,9.3692 3.78247,21.8268 0.68731,30.91061 -4.08773,11.99681 -9.48999,15.90039 -85.74647,61.95892 -39.20181,23.67768 -73.62724,43.87989 -76.50097,44.8938 -6.07113,2.14203 -19.77405,2.26137 -25.55636,0.2226 z M 81.333333,186.33581 c -46.726256,-24.8757 -54.70475,-30.43438 -61.055229,-42.53765 -3.139113,-5.98279 -3.574348,-8.34265 -3.591697,-19.4744 -0.01679,-10.77187 0.459677,-13.60465 3.185154,-18.93704 1.762692,-3.4487 5.062692,-8.036721 7.333334,-10.195591 2.270641,-2.15888 21.228438,-14.26866 42.128438,-26.91063 20.9,-12.64196 52.099997,-31.52266 69.333337,-41.95711 43.71977,-26.47145872 41.19254,-25.3020697 54.66666,-25.2951297 13.29682,0.007 2.00239,-5.250426 98,45.6166397 42.23656,22.38024 68.54114,37.10013 71.78187,40.16871 2.81336,2.66392 6.63765,7.82016 8.49844,11.45832 3.85771,7.542501 4.106,10.273131 5.49195,60.401271 0.78658,28.44929 0.65328,32.25679 -1.10559,31.5808 -1.1,-0.42275 -42.15284,-22.05419 -91.22855,-48.06987 -49.07569,-26.01566 -89.89008,-47.127021 -90.69861,-46.914121 -0.80855,0.21289 -11.26019,6.355301 -23.22585,13.649791 -19.7556,12.04336 -21.53012,13.42872 -19.30138,15.06853 1.34991,0.9932 12.05439,6.8455 23.78772,13.0051 11.73334,6.1596 25.10814,13.23073 29.72178,15.71361 l 8.38844,4.51432 -43.20223,26.21787 c -23.76121,14.41982 -43.73601,26.1612 -44.38843,26.09193 -0.65242,-0.0693 -20.68622,-10.50717 -44.519557,-23.19535 z"
                            fill="#2495f6" />
                        </svg>
                    </div>
                </div>
            </div>
        </header>
        <main class="main-content">