<?php

// Settings

add_theme_support('post-thumbnails');
add_image_size( 'blogListBig', 750, 420, true );
add_image_size( 'blogList', 420, 235, true );
add_image_size( 'teamList', 500, 500, true );

function remove_menus(){
    remove_menu_page( 'edit.php' );
    remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'remove_menus' );

// External files

function google_fonts() {
  wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&amp;subset=latin-ext', false ); 
}
add_action( 'wp_enqueue_scripts', 'google_fonts' );

function font_awesome() {
  wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false ); 
}
add_action( 'wp_enqueue_scripts', 'font_awesome' );

function add_custom_files() {
  //wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0','all' );
  wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/js/jquery.waypoints.min.js', array( 'jquery' ), '1.0', true );
  wp_enqueue_script( 'countdown', get_template_directory_uri() . '/js/jquery.countdown.min.js', array( 'jquery' ), '1.0', true );
  wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array( 'jquery', 'waypoints' ), '1.0', true );

  wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', array(), '1.0','all' );
}
add_action('wp_enqueue_scripts', 'add_custom_files');

// Menu

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

// Twitch

function twitch_stream_live($channel) {
    //$channel = 'agraelus';
    $client_id = 'e4pft3ilzteyjgu01xfxaipog9h8pe';
    $url = 'https://api.twitch.tv/kraken/streams/' . $channel . '?client_id=' . $client_id;
    $response = wp_remote_get( $url );	
    if ( !is_wp_error( $response ) && $response['response']['code'] === 200 ) {
        $body = json_decode( wp_remote_retrieve_body( $response ) );
        if ( empty( $body->stream ) ) {
            return false;
        } else {
            return true;
        }
    }
    return false;
}

// Dynamic sidebars

function components() {

  $socialSites = array(
		'name'          => 'SocialSites',
		'id'            => 'social_sites',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
		'before_widget' => '<div class="social-sites">',
		'after_widget'  => '</div>',
  );
  register_sidebar( $socialSites );

  $sponzors = array(
    'name'          => 'Sponzori',
    'id'            => 'sponzors',
    'before_title'  => '<span class="hidden">',
    'after_title'   => '</span>',
    'before_widget' => '<div class="sponzors">',
    'after_widget'  => '</div>',
    );
    register_sidebar( $sponzors );

    $language = array(
        'name'          => 'Jazyky',
        'id'            => 'language',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
        'before_widget' => '<div class="language">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $language );

    $about = array(
        'name'          => 'ONas',
        'id'            => 'about',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
        'before_widget' => '<div class="about">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $about );

    $pocetZlatych = array(
        'name'          => 'PocetZlatych',
        'id'            => 'pocetZlatych',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
        'before_widget' => '<div class="pocetZlatych">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $pocetZlatych );

    $pocetStribrnych = array(
        'name'          => 'PocetStribrnych',
        'id'            => 'pocetStribrnych',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
        'before_widget' => '<div class="pocetStribrnych">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $pocetStribrnych );

    $pocetBronzovych = array(
        'name'          => 'PocetBronzovych',
        'id'            => 'pocetBronzovych',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
        'before_widget' => '<div class="pocetBronzovych">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $pocetBronzovych );

    $pocetZapasu = array(
        'name'          => 'PocetZapasu',
        'id'            => 'pocetZapasu',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
        'before_widget' => '<div class="pocetZapasu">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $pocetZapasu );

    $curator = array(
        'name'          => 'Curator',
        'id'            => 'curator',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
        'before_widget' => '<div class="curator-region">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $curator );

    $oNasGalerie = array(
        'name'          => 'ONasGalerie',
        'id'            => 'oNasGalerie',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
        'before_widget' => '<div class="oNasGalerie">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $oNasGalerie );

    $contactForm = array(
        'name'          => 'ContactForm',
        'id'            => 'contactForm',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
        'before_widget' => '<div class="contactForm">',
        'after_widget'  => '</div>',
    );
    register_sidebar( $contactForm );

}
add_action( 'init', 'components' );

// Blog

function create_posttype() {
    register_post_type( 'blog',
      array(
        'labels' => array(
          'name' => __( 'Blog' ),
          'singular_name' => __( 'Blog' )
        ),
        'public' => true,
        'has_archive' => true,
        'taxonomies' => array('blogcat', 'post_tag' ),
        'rewrite' => array('slug' => 'news'),
        'supports' => array('title','editor','thumbnail','custom-fields','excerpt')
      )
    );
}
add_action( 'init', 'create_posttype' );

function create_blogcategory_taxonomy() {
    $labels = array(
        'name' => _x( 'Kategorie', 'taxonomy general name' ),
    );
     
    register_taxonomy('blogcat','blog', array(
        'label' => __('Blog Category'),
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'news-category' ),
    ));
}
add_action( 'init', 'create_blogcategory_taxonomy', 0 );

// Events

function create_event_postype() {
    register_post_type( 'events',
      array(
        'labels' => array(
          'name' => __( 'Události' ),
          'singular_name' => __( 'Events' )
        ),
        'public' => true,
        'has_archive' => true,
        'taxonomies' => array('eventcat', 'post_tag' ),
        'rewrite' => array('slug' => 'events'),
        //'register_meta_box_cb' => 'wpt_add_metaboxes',
        'supports' => array('title','editor','thumbnail','excerpt')
      )
    );
}
add_action( 'init', 'create_event_postype' );

function create_eventcategory_taxonomy() {
    $labels = array(
        'name' => _x( 'Kategorie', 'taxonomy general name' ),
    );
     
    register_taxonomy('eventcat','event', array(
        'label' => __('Event Category'),
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'events-category' ),
    ));
}
add_action( 'init', 'create_eventcategory_taxonomy', 0 );

// Channels

function create_channels_postype() {
    register_post_type( 'channels',
      array(
        'labels' => array(
          'name' => __( 'Streamy' ),
          'singular_name' => __( 'Channels' )
        ),
        'public' => true,
        'has_archive' => true,
        //'taxonomies' => array('eventcat', 'post_tag' ),
        'rewrite' => array('slug' => 'channels'),
        'register_meta_box_cb' => 'metaboxes',
        'supports' => array('title','thumbnail')
      )
    );
}
add_action( 'init', 'create_channels_postype' );

// Teams

function create_teams_postype() {
    register_post_type( 'teams',
      array(
        'labels' => array(
          'name' => __( 'Hráči' ),
          'singular_name' => __( 'Players' )
        ),
        'public' => true,
        'has_archive' => true,
        'taxonomies' => array('teamscat', 'post_tag' ),
        'rewrite' => array('slug' => 'teams'),
        //'register_meta_box_cb' => 'wpt_add_metaboxes',
        'supports' => array('title','editor','thumbnail','excerpt')
      )
    );
}
add_action( 'init', 'create_teams_postype' );

function create_teamscategory_taxonomy() {
    $labels = array(
        'name' => _x( 'Týmy', 'taxonomy general name' ),
    );
     
    register_taxonomy('teamscat','teams', array(
        'label' => __('Teams Category'),
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'teams-category' ),
    ));
}
add_action( 'init', 'create_teamscategory_taxonomy', 0 );

// Oceneni

function create_oceneni_postype() {
    register_post_type( 'oceneni',
      array(
        'labels' => array(
          'name' => __( 'Ocenění' ),
          'singular_name' => __( 'Ocenění' )
        ),
        'public' => true,
        'has_archive' => true,
        'taxonomies' => array('ocenenicat', 'post_tag' ),
        'rewrite' => array('slug' => 'oceneni'),
        'register_meta_box_cb' => 'metaboxes',
        'supports' => array('title','thumbnail')
      )
    );
}
add_action( 'init', 'create_oceneni_postype' );

function create_ocenenicategory_taxonomy() {
    $labels = array(
        'name' => _x( 'Kategorie', 'taxonomy general name' ),
    );
     
    register_taxonomy('ocenenicat','oceneni', array(
        'label' => __('Ocenění kategorie'),
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'oceneni-category' ),
    ));
}
add_action( 'init', 'create_ocenenicategory_taxonomy', 0 );

// Galerie

function create_galerie_postype() {
    register_post_type( 'galerie',
      array(
        'labels' => array(
          'name' => __( 'Galerie' ),
          'singular_name' => __( 'Galerie' )
        ),
        'public' => true,
        'has_archive' => true,
        'taxonomies' => array('galeriecat', 'post_tag' ),
        'rewrite' => array('slug' => 'gallery'),
        'register_meta_box_cb' => 'metaboxes',
        'supports' => array('title','editor','thumbnail')
      )
    );
}
add_action( 'init', 'create_galerie_postype' );

function create_galeriecategory_taxonomy() {
    $labels = array(
        'name' => _x( 'Kategorie', 'taxonomy general name' ),
    );
     
    register_taxonomy('galeriecat','galerie', array(
        'label' => __('Galerie kategorie'),
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'gallery-category' ),
    ));
}
add_action( 'init', 'create_galeriecategory_taxonomy', 0 );

// Metabox

function metaboxes() {
    add_meta_box( 
        'channel_name', 
        'Jméno kanálu', 
        'channel_name_callback', 
        'channels', 
        'normal', 
        'default' 
    );

    add_meta_box( 
        'channel_url', 
        'URL kanálu', 
        'channel_url_callback', 
        'channels', 
        'normal', 
        'default' 
    );

    add_meta_box( 
        'event_type', 
        'Typ události', 
        'event_type_callback', 
        'events', 
        'normal', 
        'default' 
    );

    add_meta_box( 
        'teams_oceneni', 
        'Kategorie ocenění', 
        'teams_oceneni_callback', 
        'teams', 
        'normal', 
        'default' 
    );

    add_meta_box( 
        'teams_facebook', 
        'Facebook', 
        'teams_facebook_callback', 
        'teams', 
        'normal', 
        'default' 
    );

    add_meta_box( 
        'teams_twitter', 
        'Twitter', 
        'teams_twitter_callback', 
        'teams', 
        'normal', 
        'default' 
    );

    add_meta_box( 
        'teams_instagram', 
        'Instagram', 
        'teams_instagram_callback', 
        'teams', 
        'normal', 
        'default' 
    );

    add_meta_box( 
        'teams_youtube', 
        'Youtube', 
        'teams_youtube_callback', 
        'teams', 
        'normal', 
        'default' 
    );

    add_meta_box( 
        'oceneni_place', 
        'Umístění', 
        'oceneni_place_callback', 
        'oceneni', 
        'normal', 
        'default' 
    );

    add_meta_box( 
        'oceneni_gallery', 
        'Odkaz na galerii', 
        'oceneni_gallery_callback', 
        'oceneni', 
        'normal', 
        'default' 
    );
}
add_action( 'add_meta_boxes', 'metaboxes' );

function channel_name_callback() {
	global $post;
	wp_nonce_field( basename( __FILE__ ), 'channel_fields' );
	$channelName = get_post_meta( $post->ID, 'channelName', true );
	echo '<input type="text" name="channelName" value="' . esc_textarea( $channelName )  . '" class="widefat">';
}

function channel_url_callback() {
	global $post;
	wp_nonce_field( basename( __FILE__ ), 'channel_fields' );
	$channelUrl = get_post_meta( $post->ID, 'channelUrl', true );
	echo '<input type="text" name="channelUrl" value="' . esc_textarea( $channelUrl )  . '" class="widefat">';
}

function event_type_callback() {
	global $post;
	wp_nonce_field( basename( __FILE__ ), 'event_fields' );
	$eventType = get_post_meta( $post->ID, 'eventType', true );
	echo '<input type="text" name="eventType" value="' . esc_textarea( $eventType )  . '" class="widefat">';
}

function oceneni_place_callback() {
	global $post;
	wp_nonce_field( basename( __FILE__ ), 'oceneni_fields' );
	$oceneniPlace = get_post_meta( $post->ID, 'oceneniPlace', true );
	echo '<input type="text" name="oceneniPlace" value="' . esc_textarea( $oceneniPlace )  . '" class="widefat">';
}

function oceneni_gallery_callback() {
	global $post;
	wp_nonce_field( basename( __FILE__ ), 'oceneni_fields' );
	$oceneniGallery = get_post_meta( $post->ID, 'oceneniGallery', true );
	echo '<input type="text" name="oceneniGallery" value="' . esc_textarea( $oceneniGallery )  . '" class="widefat">';
}

function teams_oceneni_callback() {
	global $post;
	wp_nonce_field( basename( __FILE__ ), 'teams_fields' );
	$teamsOceneni = get_post_meta( $post->ID, 'teamsOceneni', true );
	echo '<input type="text" name="teamsOceneni" value="' . esc_textarea( $teamsOceneni )  . '" class="widefat">';
}

function teams_facebook_callback() {
	global $post;
	wp_nonce_field( basename( __FILE__ ), 'teams_fields' );
	$teamsFacebook = get_post_meta( $post->ID, 'teamsFacebook', true );
	echo '<input type="text" name="teamsFacebook" value="' . esc_textarea( $teamsFacebook )  . '" class="widefat">';
}

function teams_twitter_callback() {
	global $post;
	wp_nonce_field( basename( __FILE__ ), 'teams_fields' );
	$teamsTwitter = get_post_meta( $post->ID, 'teamsTwitter', true );
	echo '<input type="text" name="teamsTwitter" value="' . esc_textarea( $teamsTwitter )  . '" class="widefat">';
}

function teams_instagram_callback() {
	global $post;
	wp_nonce_field( basename( __FILE__ ), 'teams_fields' );
	$teamsInstagram = get_post_meta( $post->ID, 'teamsInstagram', true );
	echo '<input type="text" name="teamsInstagram" value="' . esc_textarea( $teamsInstagram )  . '" class="widefat">';
}

function teams_youtube_callback() {
	global $post;
	wp_nonce_field( basename( __FILE__ ), 'teams_fields' );
	$teamsYoutube = get_post_meta( $post->ID, 'teamsYoutube', true );
	echo '<input type="text" name="teamsYoutube" value="' . esc_textarea( $teamsYoutube )  . '" class="widefat">';
}

function wpt_save_meta( $post_id, $post ) {
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
    }

    $array_meta['channelName'] = esc_textarea( $_POST['channelName'] );
    $array_meta['channelUrl'] = esc_textarea( $_POST['channelUrl'] );
    $array_meta['eventType'] = esc_textarea( $_POST['eventType'] );
    $array_meta['oceneniPlace'] = esc_textarea( $_POST['oceneniPlace'] );
    $array_meta['oceneniGallery'] = esc_textarea( $_POST['oceneniGallery'] );
    $array_meta['teamsOceneni'] = esc_textarea( $_POST['teamsOceneni'] );
    $array_meta['teamsInstagram'] = esc_textarea( $_POST['teamsInstagram'] );
    $array_meta['teamsTwitter'] = esc_textarea( $_POST['teamsTwitter'] );
    $array_meta['teamsFacebook'] = esc_textarea( $_POST['teamsFacebook'] );
    $array_meta['teamsYoutube'] = esc_textarea( $_POST['teamsYoutube'] );
    
	foreach ( $array_meta as $key => $value ) :
		if ( 'revision' === $post->post_type ) {
			return;
		}
		if ( get_post_meta( $post_id, $key, false ) ) {
			update_post_meta( $post_id, $key, $value );
		} else {
			add_post_meta( $post_id, $key, $value);
		}
		if ( ! $value ) {
			delete_post_meta( $post_id, $key );
		}
	endforeach;
}
add_action( 'save_post', 'wpt_save_meta', 1, 2 );

remove_shortcode('gallery');
add_shortcode('gallery', 'custom_size_gallery');

function custom_size_gallery($attr) {
    $attr['size'] = 'blogList';
    return gallery_shortcode($attr);
}

function show_future_posts($posts)
{
   global $wp_query, $wpdb;
   if(is_single() && $wp_query->post_count == 0)
   {
      $posts = $wpdb->get_results($wp_query->request);
   }
   return $posts;
}
add_filter('the_posts', 'show_future_posts');

;?>